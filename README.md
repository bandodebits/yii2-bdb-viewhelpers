Grid View Helper para o Yii2
============================
Grid View Helper para o Yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-gridview-helper "*"
```

or add

```
"bandodebits/yii2-bdb-gridview-helper": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\gridViewHelper\AutoloadExample::widget(); ?>```