<?php

namespace bdb\viewhelpers;


use ReflectionClass;

use Yii;
use yii\web\JsExpression;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\datecontrol\DateControl;

use bdb\component\Util;
use bdb\component\Debug;
use bdb\input\Masked;

use common\models\EventColorPallet;

class GridViewHelper
{
    public static $phpTypeToFilterTypeDef = ['string' => 'FILTER_TYPEAHEAD'];

    public static $normalizeFilterTypeDef = [
        'FIELD_TEXT_INPUT' => 'FILTER_TYPEAHEAD',
        'FIELD_ENUM_DROPDOWN' => 'FILTER_TYPEAHEAD',
        'FIELD_HIDDEN' => 'FILTER_TYPEAHEAD',
        'FIELD_DROPDOWN' => 'FILTER_SELECT2',
        'FILTER_COLOR_PALETTE' => 'FILTER_COLOR',
        'FILTER_COLOR_PICKER' => 'FILTER_COLOR'
    ];

    public static $filterTypeDefReflectionTargetClass = [
        'FILTER_TYPEAHEAD' => GridView::class,
        'FILTER_SELECT2' => GridView::class,
        'FILTER_TEXT_INPUT' => GridView::class,
        'FILTER_COLOR' => GridView::class,
        'FILTER_MASK' => null,
        'FILTER_ENUM' => null,
        'FILTER_SWITCH' => null,
        'FILTER_DATE_RANGE' => GridView::class,
    ];

    protected static $ignoreOverrides = [
        'limit', 'filterTypeDef', 'attribute', 'value', 'label', 'filter', 'filterWidgetOptions', 'format'
    ];

    static public function column($field, $dataProvider, $searchModel, $modelStateUrlParams, $overrides = [])
    {
        return self::flatGridViewColumn($field, $dataProvider, $searchModel, $modelStateUrlParams, $overrides);
    }

    static public function flatGridViewColumn($field, $dataProvider, $searchModel, $modelStateUrlParams, $overrides = [])
    {
        $pickOverride = function ($key, $orElse) use (&$overrides) {
            return isset($overrides[$key]) ? $overrides[$key] : (is_callable($orElse) ? $orElse() : $orElse);
        };

        $metadata = $dataProvider->query->metadata;
        $ref = $metadata->getFieldRef($field);
        $columnMetadata = $ref['columnMetadata'];
        $limit = isset($overrides['limit']) ? $overrides['limit'] : 15;
        $filterTypeDef = null;
        $commentMeta = null;

        //OBS: bypass here to get comment metada from parent relation when tsuui latter
        if (!Util::endsWith($field, '_tsuuid') && !empty($columnMetadata->comment)) {
            $commentMeta = json_decode($columnMetadata->comment);
            $filterTypeDef = $commentMeta->filterType;
        }

        if ($filterTypeDef == 'FIELD_BLOCK') {
            throw new InvalidParamException("Denied exhibition of FIELD_BLOCK, $field");
        } else if ($ref->keyType == 'id') {
            throw new InvalidParamException("Denied exhibition of an 'id' field, $field");
        }

        if (isset($overrides['filterTypeDef'])) {
            $filterTypeDef = $overrides['filterTypeDef'];
        } else {
            if ($filterTypeDef == null) {
                if ($ref->keyType == 'tsuuid' && strpos($field, '_') !== false) {
                    $entityRef = $metadata->aliases[$ref->entityAlias];
                    $parentRelationFields = $entityRef->parentRelationFields;
                    $parentEntityRef = $metadata->aliases[$entityRef->parentEntityAlias];
                    $pk = null;
                    $fk = null;

                    foreach ($parentRelationFields as $_pk => $_fk) {
                        $pk = $_pk;
                        $fk = $_fk;
                    }

                    $fkColumnsMetadata = $parentEntityRef->columnsMetadata[$fk];
                    $columnComment = $fkColumnsMetadata->comment;

                    if (!empty($columnComment)) {
                        $commentMeta = json_decode($columnComment);
                        $filterTypeDef = $commentMeta->filterType;
                    }
                } else {
                    $phpType = $columnMetadata->phpType;

                    if (isset(GridViewHelper::$phpTypeToFilterTypeDef[$phpType])) {
                        $filterTypeDef = GridViewHelper::$phpTypeToFilterTypeDef[$phpType];
                    } else {
                        throw new InvalidConfigException("Not implemented in flatGridViewColumn filterTypeDef: phpType ${$phpType}");
                    }
                }
            }
        }

        $filterTypeDefNormalized = null;
        $filterTypeDefReflectionTargetClassNormalized = null;

        if (array_key_exists($filterTypeDef, GridViewHelper::$normalizeFilterTypeDef)) {
            $filterTypeDefNormalized = GridViewHelper::$normalizeFilterTypeDef[$filterTypeDef];
        } else {
            $filterTypeDefNormalized = $filterTypeDef;
        }

        if (array_key_exists($filterTypeDefNormalized, GridViewHelper::$filterTypeDefReflectionTargetClass)) {
            $filterTypeDefReflectionTargetClassNormalized = GridViewHelper::$filterTypeDefReflectionTargetClass[$filterTypeDefNormalized];

            if ($filterTypeDefReflectionTargetClassNormalized != null) {
                $reflection = new ReflectionClass($filterTypeDefReflectionTargetClassNormalized);
                $filterType = $reflection->getConstants()[$filterTypeDefNormalized];
            }
        } else {
            throw new InvalidConfigException("Filter class not configured in the array filterTypeDefReflectionTargetClass '$filterTypeDefNormalized'");
        }


        $result = [
            'attribute' => $pickOverride('attribute', $field),
            'noWrap' => $pickOverride('noWrap', true),
        ];

        if (isset($overrides['filter'])) {
            $result['filter'] = $overrides['filter'];
        }

        if (isset($overrides['value'])) {
            $result['value'] = $overrides['value'];
        }

        if (isset($overrides['label'])) {
            $result['label'] = Yii::t('app', $overrides['label']);
        }

        switch ($filterTypeDefNormalized) {
            case 'FILTER_SWITCH':
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }
                if ($commentMeta != null && $commentMeta->filterTypeOptions != null && $commentMeta->filterTypeOptions->type != null) {
                    if ($commentMeta->filterTypeOptions->type == "SELECT2") {
                        $result['filterType'] = GridView::FILTER_SELECT2;
                        $result['filterWidgetOptions'] = [
                            'pluginOptions' => ['allowClear' => true]
                        ];
                        $result['filter'] = [0 => $commentMeta->filterTypeOptions->off, 1 => $commentMeta->filterTypeOptions->on];
                    } else {
                        throw new InvalidConfigException("Switch must be setted");
                    }
                } else {
                    throw new InvalidConfigException("Switch must be setted");
                }
                break;
            case 'FILTER_DATE_RANGE':
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }

                if ($commentMeta != null) {
                    $result['width'] = '200px';
                    $result['format'] = $pickOverride('format', ['datetime']);
                    $result['class'] = 'kartik\grid\EditableColumn';

                    $result['editableOptions'] = [
                        'placement' => 'left',
                        'header' => 'asdasd',
                        'inputType' => \kartik\editable\Editable::INPUT_DATETIME,
                        'options' => [
                            'pluginOptions' => [
                                'min' => 0, 'max' => 5000,
                                'placement' => 'top'
                            ]
                        ]
                    ];
                    //------------------------------

                    $result['filterWidgetOptions'] = [
                        'pluginOptions' => [
                            'hideInput' => true,
                            'convertFormat' => true,
                            'opens' => 'left'
                        ],
                    ];
                } else {
                    throw new InvalidConfigException("Date Range must be setted");
                }
                break;
            case 'FILTER_ENUM':
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }

                $result['filterType'] = GridView::FILTER_SELECT2;

                if ($commentMeta != null) {
                    $result['filterWidgetOptions'] = [
                        'pluginOptions' => ['allowClear' => true]
                    ];
                    $result['filter'] = \bdb\input\Enum::getData(NULL, NULL, $columnMetadata->enumValues);
                } else {
                    throw new InvalidConfigException("Enum must be setted");
                }

                break;
            case 'FILTER_MASK':
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }

                $result['filterType'] = '\yii\widgets\MaskedInput';

                if ($commentMeta != null && $commentMeta->filterTypeOptions != null && $commentMeta->filterTypeOptions->mask != null) {
                    if (!isset($result['filterInputOptions'])) {
                        $result['filterInputOptions'] = ['placeholder' => Yii::t('app', '...'), 'class' => 'form-control'];
                    }

                    if (!isset($result['filterWidgetOptions'])) {
                        $result['filterWidgetOptions'] = [
                            'mask' => $commentMeta->filterTypeOptions->mask,
                            'clientOptions' => ['clearIncomplete' => true]
                        ];
                    }
                } else {
                    throw new InvalidConfigException("Mask must be setted");
                }

                break;
            case 'FILTER_COLOR':
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }

                if (!isset($result['filterInputOptions'])) {
                    $result['filterInputOptions'] = ['placeholder' => Yii::t('app', '...'), 'class' => 'form-control'];
                }

                if (!isset($result['filterWidgetOptions'])) {
                    if ($filterTypeDef == 'FILTER_COLOR_PALETTE') {
                        $result['filterWidgetOptions'] = [
                            'showDefaultPalette' => false,
                            'pluginOptions' => [
                                'showPalette' => true,
                                'showPaletteOnly' => true,
                                'showSelectionPalette' => true,
                                'showAlpha' => false,
                                'allowEmpty' => false,
                                'preferredFormat' => 'name',
                                'palette' => [array_values(ArrayHelper::map(EventColorPallet::find()->all(), 'id', 'rgba'))]
                            ]
                        ];
                    } else if ($filterTypeDef == 'FILTER_COLOR_PICKER') {
                        $result['filterWidgetOptions'] = [
                            'showDefaultPalette' => false,
                            'pluginOptions' => [
                                'showPalette' => false,
                                'showPaletteOnly' => false,
                                'showSelectionPalette' => true,
                                'showAlpha' => false,
                                'allowEmpty' => false,
                                'preferredFormat' => 'name'
                            ]
                        ];
                    } else {
                        throw new InvalidConfigException("Must be FILTER_COLOR_PALETTE or FILTER_COLOR_PICKER");
                    }
                }
                break;
            case 'FILTER_TYPEAHEAD';
                if (!isset($result['label'])) {
                    $result['label'] = Yii::t('app', $ref['label']);
                }

                $result['filterWidgetOptions'] = $pickOverride('filterWidgetOptions', [
                    'pluginOptions' => ['highlight' => true, 'allowClear' => true],
                    'dataset' => [
                        [
                            'remote' => Url::to(['omnisearch']) . "?-t=typeahead&-f=$field&-p=1&-l=$limit&-s=%QUERY&" . $modelStateUrlParams,
                            'limit' => $limit,
                            'displayKey' => 'value'
                        ]
                    ],
                ]);
                break;
            case 'FILTER_SELECT2':
                $valueField = null;

                if (!isset($result['value'])) {
                    $textField = null;
                    $textField = 'name';

                    if (strpos($field, '_') !== false) {
                        $fieldParts = explode("_", $field);
                        $alias = $fieldParts[0];
                        $valueField = $alias . '_' . $textField;
                    } else {
                        $valueField = $textField;
                    }


                    $result['value'] = $valueField;

                    if (!isset($result['label'])) {
                        $valueFieldRef = $metadata->getFieldRef($valueField);
                        $result['label'] = Yii::t('app', $valueFieldRef->label);
                    }
                }

                $resultsJs = "
                    function (data, params) {
                        params['-p'] = params['-p'] || 1;

                        return {
                            results: data.results,
                            pagination: {
                                more: (params['-p'] * $limit) < data.totalCount
                            }
                        };
                    }
                ";

                $result['filterWidgetOptions'] = $pickOverride('filterWidgetOptions', [
                    'initValueText' => $valueField != null ? $searchModel->getFromKeyValueField($dataProvider->query, $field, $valueField) : '',
                    'pluginOptions' => [
                        // 'allowClear' => true,
                        'minimumInputLength' => 0,
                        /*
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        */
                        'ajax' => [
                            'url' => Url::to(['omnisearch']) . "?-t=dropdown&-f=$valueField&-l=$limit&" . $modelStateUrlParams,
                            'dataType' => 'json',
                            'delay' => 500,
                            'cache' => true,
                            'data' => new JsExpression('function(params) { return {"-s":params.term};}'),
                            'processResults' => new JsExpression($resultsJs)
                        ]
                    ],
                    'pluginEvents' => [
                        /* "change" => "function(evt) { console.log(evt); return false;}"*/
                    ]
                ]);
                break;
            default:
                throw new InvalidConfigException("Not implemented in flatGridViewColumn filterTypeDef: $filterTypeDefNormalized");
        }

        if (!isset($result['format'])) {
            $result['format'] = $pickOverride('format', 'raw');
        }

        if (!isset($result['filterType']) && $filterType != null) {
            $result['filterType'] = $pickOverride('filterType', $filterType);
        }

        if (!isset($result['filterInputOptions'])) {
            $result['filterInputOptions'] = $pickOverride('filterInputOptions', [
                'placeholder' => Yii::t('app', $pickOverride('placeholder', $filterTypeDef))
            ]);
        }

        foreach ($overrides as $k => $v) {
            if (!in_array($k, self::$ignoreOverrides) && !isset($result[$k])) {
                $result[$k] = $v;
            }
        }

        return $result;
    }
}

